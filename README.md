# books_lib

## Getting started

Book library for TD and pythonist

### Common python
Production Pipeline Fundamentals for Film and Games
https://www.amazon.de/-/en/Renee-Dunlop/dp/0415812291/ref=sr_1_8?crid=16HZV4JXMVB7D&keywords=Pipeline&qid=1660557089&rnid=186606&s=books&sprefix=pipeli%2Cstripbooks%2C565&sr=1-8


### Maya
Beginning 3D Game Assets Development Pipeline: Learn to Integrate from Maya to Unity
https://www.amazon.de/-/en/Nova-Villanueva/dp/1484271955/ref=sr_1_9?crid=16HZV4JXMVB7D&keywords=Pipeline&qid=1660477057&s=books&sprefix=pipeli%2Cstripbooks%2C565&sr=1-9

Maya Programming with Python Cookbook
https://www.amazon.com/Programming-Python-Cookbook-Adrian-Herbez/dp/1785283987/ref=sr_1_1?keywords=maya+python&qid=1660595869&s=books&sr=1-1

Practical Maya Programming with Python
https://www.amazon.com/Practical-Programming-Python-Robert-Galanakis-ebook/dp/B00M6N19YK/ref=sr_1_3?keywords=maya+python&qid=1660595869&s=books&sr=1-3

### blender
Taking Blender to the Next Level: Implement advanced workflows such as geometry nodes, simulations, and motion tracking for Blender production pipelines
https://www.amazon.de/-/en/Ruan-Lotter/dp/1803233567/ref=sr_1_41?crid=16HZV4JXMVB7D&keywords=Pipeline&qid=1660557180&rnid=186606&s=books&sprefix=pipeli%2Cstripbooks%2C565&sr=1-41

The Blender Python API: Precision 3D Modeling and Add-on Development
https://www.amazon.com/Blender-Python-API-Add-Development-ebook/dp/B0721S6G8Q/ref=sr_1_22?crid=3QWVRRQE2D1Z7&keywords=blender&qid=1660557524&rnid=283155&s=books&sprefix=blende%2Cstripbooks-intl-ship%2C185&sr=1-22

Shading, Lighting, and Rendering with Blender EEVEE: Create amazing concept art 12 times faster using a real-time rendering engine
https://www.amazon.com/Shading-Lighting-Rendering-Blender-EEVEE/dp/1803230967/ref=sr_1_23?crid=3QWVRRQE2D1Z7&keywords=blender&qid=1660557524&rnid=283155&s=books&sprefix=blende%2Cstripbooks-intl-ship%2C185&sr=1-23

Professional 3D Workflows with Blender: Discover tips, tricks, and hacks for using Blender in every part of the 3D digital content creation process
https://www.amazon.com/Professional-3D-Workflows-Blender-Discover-ebook/dp/B0B39DR3L7/ref=sr_1_27?crid=3QWVRRQE2D1Z7&keywords=blender&qid=1660557524&rnid=283155&s=books&sprefix=blende%2Cstripbooks-intl-ship%2C185&sr=1-27

3D Scientific Visualization with Blender (IOP Concise Physics)
https://www.amazon.com/Scientific-Visualization-Blender-Concise-Physics-ebook/dp/B09CLTFHB2/ref=sr_1_81?crid=3QWVRRQE2D1Z7&keywords=blender&qid=1660557824&rnid=283155&s=books&sprefix=blende%2Cstripbooks-intl-ship%2C185&sr=1-81

Core Blender Development: Understanding the Essential Source Code
https://www.amazon.com/Core-Blender-Development-Understanding-Essential-ebook/dp/B08QF7273F/ref=sr_1_99?crid=3QWVRRQE2D1Z7&keywords=blender&qid=1660558365&rnid=283155&s=books&sprefix=blende%2Cstripbooks-intl-ship%2C185&sr=1-99

Python Scripting in Blender for Humans
https://www.amazon.com/Python-Scripting-Blender-Christopher-Topalian-ebook/dp/B0975T9G15/ref=sr_1_103?crid=3QWVRRQE2D1Z7&keywords=blender&qid=1660558365&rnid=283155&s=books&sprefix=blende%2Cstripbooks-intl-ship%2C185&sr=1-103

The Blender Python API
https://www.amazon.com/Blender-Python-API-Chris-Conlan-ebook/dp/B07C8JFWNG/ref=sr_1_132?crid=3QWVRRQE2D1Z7&keywords=blender&qid=1660595616&rnid=283155&s=books&sprefix=blende%2Cstripbooks-intl-ship%2C185&sr=1-132

The Blender Python API: Precision 3D Modeling and Add-on Development
https://www.amazon.com/Blender-Python-API-Add-Development-ebook/dp/B0721S6G8Q/ref=sr_1_1?keywords=blender+python&qid=1660595766&s=books&sr=1-1

Learning Blender Python: A Beginner's First Steps in Understanding Blender Python
https://www.amazon.com/Learning-Blender-Python-Beginners-Understanding/dp/B084QLDY5Z/ref=sr_1_2?keywords=blender+python&qid=1660595766&s=books&sr=1-2

The Warrior-Poet's Guide to Python and Blender 2.80
https://www.amazon.com/Warrior-Poets-Guide-Python-Blender-2-80-ebook/dp/B07Z7VQBRH/ref=sr_1_7?keywords=blender+python&qid=1660595766&s=books&sr=1-7

### adobe
Automating After Effects: Scripting Techniques for the Production Pipeline with JavaScript and Python
https://www.amazon.com/Automating-After-Effects-Techniques-Production/dp/B084DG27W8/ref=sr_1_2?crid=HLP65A5EX53W&keywords=production+pipeline&qid=1660557301&rnid=2941120011&s=books&sprefix=production+pipeline%2Caps%2C212&sr=1-2
